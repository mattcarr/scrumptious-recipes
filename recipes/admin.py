from django.contrib import admin
from recipes.models import Recipe, Step, Measure, FoodItem, Ingredient
from tags.models import Tag

admin.site.register(Recipe)
admin.site.register(Step)
admin.site.register(Measure)
admin.site.register(FoodItem)
admin.site.register(Ingredient)
admin.site.register(Tag)